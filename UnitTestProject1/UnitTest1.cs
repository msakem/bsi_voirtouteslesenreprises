﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Text;

using System.Windows.Forms;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.IE;
using System.Threading;
using OpenQA.Selenium.Support.UI;

namespace UnitTestProject1
{
    [TestFixture]
    public class UntitledTestCase
    {
        private InternetExplorerDriver driver;
        private StringBuilder verificationErrors;
        private string baseURL;
        private bool acceptNextAlert = true;

        [SetUp]
        public void SetupTest()
        {
            var options = new InternetExplorerOptions()
            {
                IntroduceInstabilityByIgnoringProtectedModeSettings = true,
                IgnoreZoomLevel = true,
                EnableNativeEvents = false
            };


            driver = new OpenQA.Selenium.IE.InternetExplorerDriver(options);
            driver.Manage().Window.Maximize();
            baseURL = "https://www.katalon.com/";
            verificationErrors = new StringBuilder();
        }

        [TearDown]
        public void TeardownTest()
        {
            try
            {
                driver.Quit();
            }
            catch (Exception)
            {
                // Ignore errors if unable to close the browser
            }
            NUnit.Framework.Assert.AreEqual("", verificationErrors.ToString());
        }

        [Test]
        public void TheUntitledTestCaseTest()
        {
            if (Connection())
            {
                driver.Navigate().GoToUrl("https://acc.es2h.pp.s2h.corp/fr/");
                driver.FindElement(By.CssSelector("div.menu_item_w:nth-child(2) > a:nth-child(1)")).Click();

                driver.FindElement(By.CssSelector("html body div.content_w div.content div.crud_toolbar a.action_button:nth-child(4)")).Click();
         
                driver.FindElement(By.Id("acc_administrator_firstName")).Click();

                driver.FindElement(By.Id("acc_administrator_firstName")).SendKeys("DODO");
                driver.FindElement(By.Id("acc_administrator_lastName")).Click();

                driver.FindElement(By.Id("acc_administrator_lastName")).SendKeys("DODI");
                driver.FindElement(By.Id("acc_administrator_email")).Click();

                driver.FindElement(By.Id("acc_administrator_email")).SendKeys("issam.lemlih@gmail.com");
                driver.FindElement(By.Id("acc_administrator_roles_1")).Click();
                driver.FindElement(By.XPath("(.//*[normalize-space(text()) and normalize-space(.)='Administrateur'])[1]/following::button[1]")).Click();
            }
        }
        private bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }

        private bool IsAlertPresent()
        {
            try
            {
                driver.SwitchTo().Alert();
                return true;
            }
            catch (NoAlertPresentException)
            {
                return false;
            }
        }

        private string CloseAlertAndGetItsText()
        {
            try
            {
                IAlert alert = driver.SwitchTo().Alert();
                string alertText = alert.Text;
                if (acceptNextAlert)
                {
                    alert.Accept();
                }
                else
                {
                    alert.Dismiss();
                }
                return alertText;
            }
            finally
            {
                acceptNextAlert = true;
            }
        }
       
        void ConnexionafficherEmail()
        {
            if (Connection())
            {
                driver.Navigate().GoToUrl("https://acc.es2h.pp.s2h.corp/fr/");
                driver.FindElement(By.CssSelector("div.menu_item_w:nth-child(6) > div:nth-child(1)")).Click();
                driver.FindElement(By.CssSelector("div.menu_item_w:nth-child(6) > div:nth-child(2) > a:nth-child(2)")).Click();
                Thread.Sleep(2000);
            }
        }
        void VoiretoutesEntreprises()
        {
            if (Connection())
            {
                driver.Navigate().GoToUrl("https://acc.es2h.pp.s2h.corp/fr/");
                driver.FindElement(By.CssSelector("div.menu_item_w:nth-child(1)")).Click();
                driver.FindElement(By.CssSelector("html body div.content:nth-child(3) > div:nth-child(3) > a:nth-child(1)")).Click();
                Thread.Sleep(2000);
            }
        }

        bool Connection()
        {
            try
            {
                driver.Navigate().GoToUrl("https://acc.es2h.pp.s2h.corp/fr/login.html");
                driver.Navigate().GoToUrl("javascript:document.getElementById('overridelink').click()");
                driver.FindElement(By.Id("_username")).Click();
                driver.FindElement(By.Id("_username")).SendKeys("Houda.MSAKEM@s2hgroup.com");
                driver.FindElement(By.Id("_password")).Click();
                driver.FindElement(By.Id("_password")).SendKeys("Bonbon000@");
                driver.FindElement(By.Id("authenticate")).Click();
                Thread.Sleep(2000);
                return true;

            }
            catch( Exception e)
            {
                return false;
            }
        }
    }
}
